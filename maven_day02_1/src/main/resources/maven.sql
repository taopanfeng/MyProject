/*
Navicat MySQL Data Transfer

Source Server         : MySQL_localhost
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : maven

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-04-15 08:17:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('1', '大牛', '2018-03-13 09:29:30');
INSERT INTO `items` VALUES ('2', '二蛋', '2018-03-28 10:05:52');
INSERT INTO `items` VALUES ('3', '三驴', '2018-03-07 10:08:04');
INSERT INTO `items` VALUES ('4', '四毛', '2020-04-15 08:17:41');
INSERT INTO `items` VALUES ('5', '五虎', '2020-04-15 08:17:45');



