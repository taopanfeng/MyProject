package tpf.controller;

import jdk.internal.util.xml.impl.ReaderUTF8;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tpf.converter.StringToLocalDateTime;
import tpf.entity.Age;
import tpf.entity.Grade;
import tpf.entity.Student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLDecoder;
import java.util.List;

@Controller
@RequestMapping("skndxc,m/sdf,sdf/sdfsd/")
public class HelloController
{
    @RequestMapping("hello2")
    public String sayHello2(String username, String password, String Username)
    {
        System.out.println(username);
        System.out.println(password);
        System.out.println(Username);
        return "success";
    }

    @RequestMapping(value = "hello3", method = {RequestMethod.POST, RequestMethod.GET})
    public String sayHello3(Student student)
    {
        System.out.println(student);
        return "success";
    }

    //HttpServletRequest
    //HttpServletResponse
    //HttpSession
    //java.security.Principal
    //Locale
    //InputStream
    //OutputStream
    //Reader
    //Writer

    @RequestMapping("testServlet")
    public String testServlet(HttpServletRequest request, HttpServletResponse response, HttpSession session)
    {
        System.out.println(request);
        System.out.println(response);
        System.out.println(session);
        return "success";
    }

    @RequestMapping("testRequestParam")
    public String testRequestParam(/*@RequestParam*/ List<String> list)
    {
        System.out.println(list);
        return "success";
    }

    @RequestMapping("testRequestParam2")
    public String testRequestParam2(@RequestParam List<String> list)
    {
        System.out.println(list);// [大牛, 二蛋]
        return "success";
    }

    @RequestMapping("testRequestParam3")
    public String testRequestParam3(@RequestParam(value = "name",
                    required = false,
                    defaultValue = "陶攀峰") String username)
    {
        // http://localhost:8080/springmvc/testRequestParam3
        //      输出：陶攀峰【没有接收到参数name】
        // http://localhost:8080/springmvc/testRequestParam3?name=大牛
        //      输出：大牛【接收到了参数name】
        // http://localhost:8080/springmvc/testRequestParam3?username=大牛
        //      输出：陶攀峰【没有接收到参数name】
        System.out.println(username);
        return "success";
    }


    @RequestMapping(value = "testRequestParam4",method =RequestMethod.GET)
    public String testRequestParam4(@RequestParam(required = true)String username)
    {
        System.out.println(username);
        return "success";
    }

    @RequestMapping(value = "testRequestParam5",method =RequestMethod.POST)
    public String testRequestParam5(@RequestParam(required = true)String username)
    {
        System.out.println(username);
        return "success";
    }

    @RequestMapping("testRequestBody")
    public String testRequestBody(@RequestBody String username)
    {
        System.out.println(username);// 我是请求体...
        return "success";
    }






    @RequestMapping("testRequestBody2")
    @ResponseBody
    public String testRequestBody2(@RequestBody Grade grade)
    {
        System.out.println(222);
        System.out.println(grade);
        //String utf8 = URLDecoder.decode(username, "UTF8");
        //System.out.println("utf8："+utf8);// utf8：usernam=大牛
        //System.out.println(username);// usernam=%E5%A4%A7%E7%89%9B
        return "success";
    }

    @RequestMapping("testRequestBody3")
    public String testRequestBody3(@RequestBody String grade)
    {
        System.out.println(333);
        System.out.println(grade);
        //String utf8 = URLDecoder.decode(username, "UTF8");
        //System.out.println("utf8："+utf8);// utf8：usernam=大牛
        //System.out.println(username);// usernam=%E5%A4%A7%E7%89%9B
        return "success";
    }

    @RequestMapping("testRequestBody4")
    @ResponseBody
    public String testRequestBody4(@RequestBody Age age)
    {
        System.out.println(444);
        System.out.println(age);
        //String utf8 = URLDecoder.decode(username, "UTF8");
        //System.out.println("utf8："+utf8);// utf8：usernam=大牛
        //System.out.println(username);// usernam=%E5%A4%A7%E7%89%9B
        return "success";
    }

}



