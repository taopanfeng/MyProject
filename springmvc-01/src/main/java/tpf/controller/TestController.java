package tpf.controller;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import tpf.exception.MyException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

@Controller
public class TestController
{
    @RequestMapping("testInterceptor")
    public String testInterceptor(HttpServletResponse response) throws Exception
    {
        System.out.println("testInterceptor...");
        return "success";
    }

    @RequestMapping("user")
    public String user()
    {
        System.out.println("user...");
        return "hello";
    }

    @RequestMapping("user/hello")
    public String userHello()
    {
        System.out.println("userHello...");
        return "hello";
    }
    @RequestMapping("user/hello/hello")
    public String userHelloHello()
    {
        System.out.println("userHelloHello...");
        return "hello";
    }
    @RequestMapping("hello/user/hello")
    public String helloUserHello()
    {
        System.out.println("helloUserHello...");
        return "hello";
    }

    @RequestMapping("testMyException2")
    public String testMyException2() throws Exception
    {
        try {
            int a=0/0;
        }catch (Exception e) {
            throw new MyException("除数不能为0");
        }
        return "success";
    }

    @RequestMapping("testMyException1")
    public String testMyException1()
    {
        int a=0/0;
        return "success";
    }

    @RequestMapping("testFileUpload3")
    public String testFileUpload3(List<MultipartFile> files) throws Exception
    {
            int a=0/0;
        String path="http://localhost:8081/springmvc/";
        for (MultipartFile file : files)
        {
            // 获取到上传文件的名称
            String filename = file.getOriginalFilename();
            if (!filename.isEmpty())
            {
                Client client=Client.create();
                String uuid = UUID.randomUUID().toString();
                filename=URLEncoder.encode(filename,"UTF-8");
                // 编码前：大牛.jpg
                // 编码后：%E5%A4%A7%E7%89%9B.jpg
                WebResource webResource=client.resource(path+uuid+filename);
                webResource.put(file.getBytes());
                System.out.println(filename + "上传完成...");
            }
        }
        System.out.println("done...");
        return "success";
    }



    @RequestMapping("testFileUpload2")
    public String testFileUpload2(List<MultipartFile> files) throws Exception
    {
        for (MultipartFile file : files)
        {
            // 获取到上传文件的名称
            String filename = file.getOriginalFilename();
            if (!filename.isEmpty())
            {
                String uuid = UUID.randomUUID().toString();
                // 完成文件上传
                file.transferTo(new File("C:/" + uuid + filename));
                System.out.println(filename + "上传完成...");
            }
        }
        System.out.println("done...");
        return "success";
    }


    @RequestMapping("testFileUpload1")
    public String testFileUpload1(HttpServletRequest request) throws Exception
    {
        // 使用fileupload组件完成文件上传
        // 解析request对象，获取上传文件项
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        // 解析request
        List<FileItem> items = upload.parseRequest(request);
        // 遍历
        for (FileItem item : items)
        {
            // 进行判断，当前item对象是否是上传文件项
            if (item.isFormField())
            {
                // 说明普通表单向
            } else
            {
                // 说明上传文件项
                String filename = item.getName();// 获取上传文件的名称：大牛.jpg
                // 把文件的名称设置唯一值，uuid
                String uuid = UUID.randomUUID().toString();
                // 完成文件上传
                item.write(new File("C:/" + uuid + filename));
                // 删除临时文件
                item.delete();
                System.out.println(filename + "上传完成...");
            }
        }
        System.out.println("done...");
        return "success";
    }
}
