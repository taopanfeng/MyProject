package tpf.controller;

import netscape.javascript.JSObject;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import tpf.entity.Grade;
import tpf.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("sklmdlfkmsdkomfksdmnfgkdjgfdghfbvnfvhbfdd")
public class RestfulController
{

    @RequestMapping(value = "testResponseBody")
    @ResponseBody
    public Grade testResponseBody(@RequestBody Grade grade)
    {
        System.out.println(grade);
        grade.setId(2);
        grade.setName("二年级");
        return grade;
    }



    @RequestMapping(value = "testRequestBody")
    public String testRequestBody(@RequestBody Grade grade)
    {
        System.out.println(grade);// Grade{id=1, name='一年级'}
        return "success";
    }


    @RequestMapping("a")
    public String methodA()
    {
        System.out.println("a");
        return "success";
    }

    @RequestMapping("b")
    public void methodB(HttpServletRequest request,HttpServletResponse response) throws Exception
    {
        /*1. 转发*/
        //request.getRequestDispatcher("WEB-INF/pages/success.jsp").forward(request,response);

        /*2. 重定向*/
        //response.sendRedirect("a");
        //response.sendRedirect("WEB-INF/pages/success.jsp");//错误写法

        /*3. 向页面写入数据*/
        //response.setContentType("application/json;charset=utf-8");
        //response.getWriter().write("你好tpf");

    }
}




