package tpf.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class Student implements Serializable
{
    private String name;
    private int age;
    private LocalDateTime birthday;
    private List<String> list;
    private Map<String,String> map;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", list=" + list +
                ", map=" + map +
                '}';
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public LocalDateTime getBirthday()
    {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday)
    {
        this.birthday = birthday;
    }

    public List<String> getList()
    {
        return list;
    }

    public void setList(List<String> list)
    {
        this.list = list;
    }

    public Map<String, String> getMap() { return map; }

    public void setMap(Map<String, String> map) { this.map = map; }
}
