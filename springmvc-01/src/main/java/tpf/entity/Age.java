package tpf.entity;

import java.io.Serializable;

public class Age implements Serializable
{
    private int age;

    @Override
    public String toString()
    {
        return "我的年龄是：Age{" +
                "age=" + age +
                '}';
    }
}
