package tpf.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyInterceptor implements HandlerInterceptor
{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        System.out.println("前");
        //return false;// 后面全部不执行。控制台只打印：前
        request.getRequestDispatcher("WEB-INF/pages/hello.jsp").forward(request,response);//依次打印：前 hello.jsp testInterceptor... 后 success.jsp 最终
        return true;
    }

    @Override// 后执行完才执行页面
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
        System.out.println("后");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
    {
        System.out.println("最终");
    }
}




