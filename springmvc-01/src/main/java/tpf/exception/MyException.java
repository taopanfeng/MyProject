package tpf.exception;

import java.io.Serializable;

public class MyException extends Exception implements Serializable
{
    private String message;

    public MyException(String message)
    {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
