package tpf.exception;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*异常处理器*/
@Component
public class MyExceptionResolver implements HandlerExceptionResolver
{

    /*跳转到具体的错误页面的方法*/
    @Override
    public ModelAndView resolveException(
            HttpServletRequest request, HttpServletResponse response, Object o, Exception e)
    {
        MyException myException = null;
        // 获取到异常对象
        if (e instanceof MyException) {
            myException = (MyException) e;
        } else {
            myException = new MyException("请联系管理员...");
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("message", myException.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
