# MyProject
我的代码分享

+ maven_day02_1 [SSM整合案案案例2—_Items](https://www.taopanfeng.top/2020/03/22/2020-03-22...14.23.14/#SSM整合案例)

+ maven_day02_parent
[Maven改造父子工程](https://www.taopanfeng.top/2020/03/08/2020-03-08...16.14.40/#父子工程改造)
[Maven私服测试](https://www.taopanfeng.top/2020/03/08/2020-03-08...16.14.40/#私服)

+ ssm   [SSM整合案例_Accountnt_springmvc03的案例](https://www.taopanfeng.top/2020/03/22/2020-03-22...14.23.14/#SSM整合案例)

+ sankey    [Python基于pyecharts生成HTML桑基图](https://www.taopanfeng.top/2020/04/17/2020-04-17...17.31.51/)

+ SpringBoot高级  [SpringBoot](https://www.taopanfeng.top/categories/SpringBoot/)
```text
springboot-01-cache         SpringBoot与缓存
springboot-02-rabbitmq      SpringBoot与消息
springboot-03-elasticsearch SpringBoot与检索
springboot-04-task          SpringBoot与任务
springboot-05-security      SpringBoot与安全
springboot-06-dubbo         SpringBoot整合Zookeeper + Dubbo（1.5.12和2.2.2）
springboot-06-springcloud   SpringBoot整合SpringCloud（1.5.12和2.2.2）
springboot-07-hot-deploy    SpringBoot与热部署
springboot-08-actuator      SpringBoot与监控
```

+ mybatis-test	[Mybatis简单入门](https://www.taopanfeng.top/2020/03/09/2020-03-09...09.35.04/#简单入门)

+ springmvc-01	[SpringMVC小总结](https://www.taopanfeng.top/2020/03/22/2020-03-22...14.23.14/)

+ springboot-mongo	[SpringBoot整合MongoDB JPA,测试MongoRepository与MongoTemplate用法,简单增删改查+高级聚合](https://www.taopanfeng.top/2019/09/30/2019-09-30...17.05.20/)

+ cloud2020

+ drools-workbench 规则引擎案例















