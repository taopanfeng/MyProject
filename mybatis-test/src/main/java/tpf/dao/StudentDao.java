package tpf.dao;

import tpf.entity.Student;

import java.util.List;

public interface StudentDao
{
    //@Select("select * from student")//
    List<Student> findAll();

    int savaStudent(Student student);

    List<Student> find(List<Student> list);
}
