import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import tpf.dao.StudentDao;
import tpf.entity.Student;

import java.io.InputStream;
import java.util.List;

public class MyTest
{
    @Test
    public void test() throws Exception
    {
        //InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        //SqlSessionFactory factory = builder.build(in);
        //SqlSession session = factory.openSession();
        //StudentDao studentDao = session.getMapper(StudentDao.class);
        //List<Student> students = studentDao.findAll();
        //List<Student> students2 = studentDao.findAll();
        //System.out.println(students==students2);//true

        //InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        //SqlSessionFactory factory = builder.build(in);
        //SqlSession session = factory.openSession();
        //StudentDao studentDao = session.getMapper(StudentDao.class);
        //List<Student> students = studentDao.findAll();
        //session.clearCache();//清除缓存
        //List<Student> students2 = studentDao.findAll();
        //System.out.println(students==students2);//false



        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(in);
        SqlSession session = factory.openSession();
        StudentDao studentDao = session.getMapper(StudentDao.class);
        List<Student> students = studentDao.findAll();

        StudentDao studentDao2 = factory.openSession().getMapper(StudentDao.class);
        List<Student> students2 = studentDao2.findAll();
        System.out.println(students == students2);//false


    }
}
