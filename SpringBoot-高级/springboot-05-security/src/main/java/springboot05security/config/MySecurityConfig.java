package springboot05security.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter
{

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        //自定义请求的授权规则

        //.antMatchers("/").permitAll() 表示所有人可以访问首页 "/"
        //.antMatchers("/level1/**").hasRole("VIP1") 表示只有角色为VIP1 才能访问 "/level1/**"
        //.antMatchers("/level2/**").hasRole("VIP2") 表示只有角色为VIP2 才能访问 "/level2/**"
        //.antMatchers("/level3/**").hasRole("VIP3") 表示只有角色为VIP3 才能访问 "/level3/**"
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3");

        //开启自动配置的登录功能
        //usernameParameter对应 账号:<input type="text" name="diy_username"/><br> 中的name
        //passwordParameter对应 密码:<input type="password" name="diy_password"><br/> 中的name
        //loginPage对应 <form th:action="@{/userlogin}" method="post"> 中的th:action
        http.formLogin()
                .usernameParameter("diy_username")//默认 username
                .passwordParameter("diy_password")// 默认 password
                .loginPage("/userlogin");
        //1, 默认post形式的 /login 表示处理登录,来到登录页面
        //2, 重定向到 /login?error 表示登录失败
        //http.formLogin();

        //开启自动配置的注销功能,注销成功来到 "/"
        http.logout().logoutSuccessUrl("/");
        //1, 访问 /logout 表示用户注销,清空session
        //2, 注销成功会默认重定向 /login?logout 页面，可以使用logoutSuccessUrl进行更改
        //http.logout();


        //开启记住我功能
        //登录成功以后,将cookie发给浏览器保存,以后登录带上cookie,可以免登录
        //点击注销,会删除cookie
        //rememberMeParameter参数与 <input type="checkbox" name="diy_rememberme" value="记住我"><br/> name相同
        http.rememberMe().rememberMeParameter("diy_rememberme");
        //http.rememberMe();

    }

    //自定义认证规则
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        //创建密码编码器 implements PasswordEncoder 来解决 如下错误:
        //java.lang.IllegalArgumentException: There is no PasswordEncoder mapped for the id "null"
        auth.inMemoryAuthentication().passwordEncoder(new MyPasswordEncoder())
                .withUser("a").password("1").roles("VIP1").and()
                .withUser("b").password("2").roles("VIP1","VIP2").and()
                .withUser("c").password("3").roles("VIP1","VIP2","VIP3");
    }
}
