package springboot05security.config;

import org.springframework.security.crypto.password.PasswordEncoder;

//创建密码编码器 implements PasswordEncoder 来解决 如下错误:
//java.lang.IllegalArgumentException: There is no PasswordEncoder mapped for the id "null"
public class MyPasswordEncoder implements PasswordEncoder
{
    //编码
    @Override
    public String encode(CharSequence c)
    {
        return c.toString();
    }

    //匹配
    @Override
    public boolean matches(CharSequence c, String s)
    {
        return s.equals(c.toString());
    }
}
