package springboot02rabbitmq.bean;

public class Student
{
    private String stu_name;
    private String stu_age;

    public Student() {
    }

    public Student(String stu_name, String stu_age) {
        this.stu_name = stu_name;
        this.stu_age = stu_age;
    }

    public String getStu_name()
    {
        return stu_name;
    }

    public void setStu_name(String stu_name)
    {
        this.stu_name = stu_name;
    }

    public String getStu_age()
    {
        return stu_age;
    }

    public void setStu_age(String stu_age)
    {
        this.stu_age = stu_age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stu_name='" + stu_name + '\'' +
                ", stu_age='" + stu_age + '\'' +
                '}';
    }
}
