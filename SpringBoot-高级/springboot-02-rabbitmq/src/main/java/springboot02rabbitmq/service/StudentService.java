package springboot02rabbitmq.service;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import springboot02rabbitmq.bean.Student;

@Service
public class StudentService
{
    //接收方式1
    //持续监听名称为"man"的队列，有消息就会从队列中取出
    @RabbitListener(queues = "man")
    public void receive01(Student student)
    {
        System.out.println("receive01收到消息:" + student);
    }

    //接收方式2
    //@RabbitListener(queues = "man")
    public void receive02(Message message)
    {
        System.out.println("receive02收到消息:" + message);
        System.out.println("消息头:" + message.getMessageProperties());
        System.out.println("消息体:" + message.getBody());
        System.out.println();
    }

    //接收方式2
    /*
        `@RabbitListener`：方法上的注解，声明这个方法是一个消费者方法，需要指定下面的属性：
        `bindings`：指定绑定关系，可以有多个。值是`@QueueBinding`的数组。
        `@QueueBinding`包含下面属性：
            - `value`：这个消费者关联的队列。值是`@Queue`，代表一个队列
            - `exchange`：队列所绑定的交换机，值是`@Exchange`类型
            - `key`：队列和交换机绑定的`RoutingKey`
     */
    @RabbitListener(bindings = @QueueBinding(
            //value可以diy 不存在会自动创建
            value = @Queue(value = "test.queue", durable = "true"),
            //exchange需要指定存着的
            exchange = @Exchange(value = "exchange.topic", ignoreDeclarationExceptions = "true", type = ExchangeTypes.TOPIC),
            //key也就是routingKey 指定匹配规则
            key = {"diy.#"}
    ))
    public void receive03(String message)
    {
        System.out.println("receive03收到消息:" + message);
    }
}
