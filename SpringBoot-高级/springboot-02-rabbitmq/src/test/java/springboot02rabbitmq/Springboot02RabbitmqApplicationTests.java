package springboot02rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import springboot02rabbitmq.bean.Student;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Springboot02RabbitmqApplicationTests
{
    //给RabbitMQ发送和接收消息
    @Autowired
    RabbitTemplate rabbitTemplate;

    /*
     * 描述:单播
     * 【时间 2019-06-13 10:09 作者 陶攀峰】
     */

    //推送数据
    @Test
    public void send_direct()
    {
        //Message需要自己构造一个,定义消息体,消息头
        //rabbitTemplate.send(exchange,routingKey,message);

        //Object默认为消息体,只需要传入要发送的对象,自动序列化发送给RabbitMQ
        //rabbitTemplate.convertAndSend(exchange,routingKey,object);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "消息01");
        map.put("data", Arrays.asList("direct_taopanfeng.name", 123, true));
        //对象被默认序列化以后发出去
        //rabbitTemplate.convertAndSend("exchange.direct", "taopanfeng.name", map);
        Student student = new Student("大牛", "12");
        rabbitTemplate.convertAndSend("exchange.direct", "taopanfeng.name", student);
    }

    //接收数据
    @Test
    public void receive_direct()
    {
        //接收到,就从队列中移除了
        Object object = rabbitTemplate.receiveAndConvert("taopanfeng.name");
        System.out.println(object);
    }

    /*
     * 描述:广播
     * 【时间 2019-06-13 10:10 作者 陶攀峰】
     */
    @Test
    public void send_fanout()
    {
        //不用设置路由键
        Student student = new Student("三驴", "77");
        rabbitTemplate.convertAndSend("exchange.fanout", "", student);
    }


    //RabbitMQ的系统管理功能组件,创建和删除 Exchange 交换器,Binding 绑定,Queue 消息
    @Autowired
    AmqpAdmin amqpAdmin;

    @Test
    public void create_exchange()
    {
        amqpAdmin.declareExchange(new DirectExchange("diy.exchange.direct"));
        System.out.println("创建Direct_Exchange完成");
        amqpAdmin.declareExchange(new FanoutExchange("diy.exchange.fanout"));
        System.out.println("创建Fanout_Exchange完成");
        amqpAdmin.declareExchange(new TopicExchange("diy.exchange.topic"));
        System.out.println("创建Topic_Exchange完成");
    }

    @Test
    public void create_queue()
    {
        //durable 表示是持久化的,重新连接还在
        amqpAdmin.declareQueue(new Queue("diy.queue"));
        System.out.println("创建队列diy.queue完成");
    }

    @Test
    public void create_binding()
    {
        //1,destination 目的地,要绑定什么,这里是 队列名为 diy.queue
        //2,目的地类型,这里是 队列
        //3,exchange 交换器 指定绑到哪个交换器,这里是 diy.exchange.direct
        //4,routingKey 路由键,指定哪儿路由规则,这里是 diy.routingkey
        //5,arguments 参数头,类型为Map,这里设为null
        amqpAdmin.declareBinding(new Binding(
                "diy.queue",
                Binding.DestinationType.QUEUE,
                "diy.exchange.direct",
                "diy.routingkey",
                null));
        System.out.println("绑定成功~!!!");
    }

    @Test
    public void test_md_Send() throws Exception
    {
        String msg = "测试day15测试代码";
        // exchange必须指定存着的
        // routingKey可以指定存在或者不存在的
        rabbitTemplate.convertAndSend("exchange.topic", "diy.diy", msg);
        // 等待10秒后再结束
        Thread.sleep(10_000);
        System.out.println("线程sleep10秒后...结束...");
    }

}
