package springboot02rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import springboot02rabbitmq.bean.Student;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试RabbitMQ，第二次学习
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-07 15:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Test02RabbitTemplate
{
    @Autowired
    RabbitTemplate rabbitTemplate;

    /*使用默认消息转换器 SimpleMessageConverter*/
    @Test
    public void testSend() {
        //rabbitTemplate.convertAndSend(String exchange, String routingKey, Object object);

        Map<String,String> map=new HashMap<>();
        map.put("k","v");
        rabbitTemplate.convertAndSend("exchange.direct", "man", map);
    }

    @Test
    public void testReceived() {
        //rabbitTemplate.receiveAndConvert(String queueName);

        Object o = rabbitTemplate.receiveAndConvert("man");
        System.out.println(o.getClass());// class java.util.HashMap
        System.out.println(o);// {k=v}
    }

    /*要配置JSON转换器 Jackson2JsonMessageConverter*/
    @Test
    public void testJsonSend()
    {
        Student student = new Student("大牛", "18");
        rabbitTemplate.convertAndSend("exchange.direct", "man", student);
    }
}
