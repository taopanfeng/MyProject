package springboot02rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import springboot02rabbitmq.bean.Student;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试RabbitMQ，第二次学习
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-07 15:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Test02AmqpAdmin
{
    //RabbitMQ的系统管理功能组件,创建和删除 Exchange 交换器,Binding 绑定,Queue 消息
    @Autowired
    AmqpAdmin amqpAdmin;

    @Test
    public void create_exchange()
    {
        amqpAdmin.declareExchange(new DirectExchange("diy.exchange.direct"));
        System.out.println("创建Direct_Exchange完成");
        amqpAdmin.declareExchange(new FanoutExchange("diy.exchange.fanout"));
        System.out.println("创建Fanout_Exchange完成");
        amqpAdmin.declareExchange(new TopicExchange("diy.exchange.topic"));
        System.out.println("创建Topic_Exchange完成");
    }

    @Test
    public void create_queue()
    {
        //durable 表示是持久化的,重新连接还在
        amqpAdmin.declareQueue(new Queue("diy.queue"));
        System.out.println("创建队列diy.queue完成");
    }

    @Test
    public void create_binding()
    {
        //1,destination 目的地,要绑定什么,这里是 队列名为 diy.queue
        //2,目的地类型,这里是 队列
        //3,exchange 交换器 指定绑到哪个交换器,这里是 diy.exchange.direct
        //4,routingKey 路由键,指定哪儿路由规则,这里是 diy.routingkey
        //5,arguments 参数头,类型为Map,这里设为null
        amqpAdmin.declareBinding(new Binding(
                "diy.queue",
                Binding.DestinationType.QUEUE,
                "diy.exchange.direct",
                "diy.routingkey",
                null));
        System.out.println("绑定成功~!!!");
    }
}
