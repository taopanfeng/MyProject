package springboot01cache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import springboot01cache.bean.Student;
import springboot01cache.mapper.StudentMapper;
import springboot01cache.service.StudentService;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Springboot01CacheApplicationTests
{

    @Resource
    StudentMapper studentMapper;

    @Test
    public void test_mapper()
    {
        Student student = studentMapper.selectStudentById(1);
        System.out.println(student);
    }

    //K V都是字符串
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    /**
     * Redis常见的五大数据类型
     * String（字符串）、List（列表）、Set（集合）、Hash（散列）、ZSet（有序集合）
     * stringRedisTemplate.opsForValue()   [String（字符串）]
     * stringRedisTemplate.opsForList()    [List（列表）]
     * stringRedisTemplate.opsForSet()     [Set（集合）]
     * stringRedisTemplate.opsForHash()    [Hash（散列）]
     * stringRedisTemplate.opsForZSet()    [ZSet（有序集合）]
     */

    //K V 都是Object对象
    @Autowired
    RedisTemplate redisTemplate;

    @Test
    public void test_redisTemplate()
    {
        int id = 1;
        Student student = studentMapper.selectStudentById(id);
        //默认保存对象,使用JDK序列号机制,让保存的类实现Serializable接口,序列化的数据保存Redis中
        //redisTemplate.opsForValue().set("student_"+id,student);
        //将序列化转为JSON形式保存到Redis中
        //System.out.println(student);//    Student{id=1, stu_name='大牛', stu_age='12', grade_id='2'}
        redisTemplate.opsForValue().set("student_" + id, student);
    }

    @Autowired
    StudentService studentService;

    @Test
    public void test_cacheManager()
    {
        Student student = studentService.selectStudentById(2);
        System.out.println(student);//    Student(id=2, stu_name=二蛋, stu_age=76, grade_id=2)
    }
}
