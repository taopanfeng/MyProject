package springboot01cache.mapper;

import org.apache.ibatis.annotations.*;
import springboot01cache.bean.Student;

public interface StudentMapper
{

    @Select("SELECT * FROM STUDENT WHERE ID=#{id}")
    Student selectStudentById(Integer id);

    @Select("SELECT * FROM STUDENT WHERE STU_NAME=#{stu_name}")
    Student selectStudentByStu_name(String stu_name);

    @Update("UPDATE STUDENT SET STU_NAME=#{stu_name},STU_AGE=#{stu_age},GRADE_ID=#{grade_id} WHERE ID=#{id}")
    void updateStudent(Student student);

    @Delete("DELETE FROM STUDENT WHERE ID=#{id}")
    void deleteStudentById(Integer id);

    @Insert("INSERT INTO STUDENT(STU_NAME,STU_AGE,GRADE_ID) VALUES(#{stu_name},#{stu_age},#{grade_id})")
    void insertStudent(Student student);
}
