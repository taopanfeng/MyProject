package springboot01cache.mapper;

import org.apache.ibatis.annotations.Select;
import springboot01cache.bean.Grade;

public interface GradeMapper
{

    @Select("SELECT * FROM GRADE WHERE ID=#{id}")
    Grade selectGradeById(Integer id);
}
