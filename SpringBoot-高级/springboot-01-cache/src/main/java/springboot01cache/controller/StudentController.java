package springboot01cache.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import springboot01cache.bean.Student;
import springboot01cache.service.StudentService;

@RestController
public class StudentController
{
    @Autowired
    StudentService studentService;

    //查询1号学生 http://localhost:8080/selectStudentById/1
    @GetMapping("/selectStudentById/{id}")
    public Student selectStudentById(@PathVariable("id") Integer id){
        Student student=studentService.selectStudentById(id);
        return student;
    }

    //修改1号学生 http://localhost:8080/updateStudent?id=1&stu_name=大牛&stu_age=15&grade_id=2
    @GetMapping("/updateStudent")
    public Student updateStudent(Student student){
        Student return_student = studentService.updateStudent(student);
        return return_student;
    }

    //删除1号学生 http://localhost:8080/deleteStudentById/1
    @GetMapping("/deleteStudentById/{id}")
    public String deleteStudentById(@PathVariable("id") Integer id){
        studentService.deleteStudentById(id);
        return "Delete_Success~!!!";
    }

    @GetMapping("/selectStudentByStu_name/{stu_name}")
    public Student selectStudentByStu_name(@PathVariable("stu_name") String stu_name){
        return studentService.selectStudentByStu_name(stu_name);
    }



}
