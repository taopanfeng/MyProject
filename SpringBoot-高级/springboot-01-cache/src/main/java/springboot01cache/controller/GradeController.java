package springboot01cache.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import springboot01cache.bean.Grade;
import springboot01cache.service.GradeService;

@RestController
public class GradeController
{
    @Autowired
    GradeService gradeService;

    @GetMapping("/selectGradeById/{id}")
    public Grade selectGradeById(@PathVariable("id") Integer id){
        Grade grade=gradeService.selectGradeById(id);
        return grade;
    }
}
