package springboot01cache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import springboot01cache.bean.Grade;
import springboot01cache.mapper.GradeMapper;

@Service
public class GradeService
{
    @Autowired
    GradeMapper gradeMapper;

    @Cacheable(cacheNames = "grade")
    public Grade selectGradeById(Integer id){
        System.out.println("查询" + id + "年级...");
        Grade grade=gradeMapper.selectGradeById(id);
        return grade;
    }

}
