package springboot01cache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;
import springboot01cache.bean.Student;
import springboot01cache.mapper.StudentMapper;


//抽取缓存的公共配置,加在类上,方法中就不用写了
@CacheConfig(cacheNames = "student")
@Service
public class StudentService
{
    @Autowired
    StudentMapper studentMapper;

    //@Cacheable将方法的运行结果进行缓存,以后要相同的数据,直接从缓存中获取,不用调用方法了
    //CacheManager管理多个Cache组件,对缓存的真正增删改查是操作在Cache组件中的,每一个缓存组件有自己唯一一个名字
    //属性 cacheNames 或者 value ,指定缓存的名字,可指定多个名称,例如:cacheNames = "student"  或者 cacheNames = {"student","student01"}
    //属性 key ,指定缓存数据使用的key,默认是使用方法参数的值,  例如 selectStudentById()方法传入1,则key为1,value为返回的Student对象
    //属性key支持SpEL表达式 key="#root.args[0]" 其中#root.args[0] 可以换成 #p0 #a0 #iban 也可以#参数名,例如 #id
    //如果传入id为2 想让key为 selectStudentById[2] 可以写成 key = "#root.methodName+'['+#id+']'"
    //属性 keyGenerator 表示key的生成器,可以自己指定key的生成器的组件id, key 和 keyGenerator 不能同时用
    //keyGenerator = "myKeyGenerator" 自定义生成方式 config.MyCacheConfig.keyGenerator()方法的@Bean的id
    //属性 cacheManager  缓存管理器 cacheResolver 缓存解析器 不能同时用
    //属性 condition 指定符合条件的情况下才缓存,也可用SpEL表达式,例如 condition="#id>0" 表示 传入参数 id 的值大于0才能进行缓存
    //属性 unless 符合条件不缓存,可以使用#result获取结果进行判断,例如 unless="#result==null" 如果结果为null就不进行缓存
    //condition = "#id==3",unless = "#id==3" id为什么都不缓存
    //condition = "#id>1",unless = "#id==3"  id<=1或id==3不缓存,其他时候都缓存
    //属性 sync 是否使用异步模式 默认false,异步模式不支持unless属性
    @Cacheable(/*cacheNames = "student"*/)
    public Student selectStudentById(Integer id)
    {
        System.out.println("查询" + id + "号学生...");
        Student student = studentMapper.selectStudentById(id);
        return student;
    }

    //@CachePut 既调用方法更新了数据库数据,又更新了缓存数据
    //运行顺序,先调用目标方法,再把目标方法的结果缓存起来
    //这里的key = "#student.id" 和 key = "#result.id" 是一样的
    //但是@Cacheable中的key不能用#result,因为@Cacheable是方法调用之前进行缓存,@CachePut是方法调用之后进行缓存
    //1,第一次查询1号学生,缓存没有key为1,调用查询方法,第二次查询1号学生,缓存中有有key为1,不调用查询方法
    //2,更新了1号学生信息,先更新数据库为1号的学生信息,再对缓存数据key为1的数据进行更新
    //3,更新了数据库和缓存信息之后,第三次查询1号学生,缓存中有key为1,不调用查询方法,查询key为1的缓存信息,显示出来
    @CachePut(/*cacheNames = "student", */key = "#result.id")
    public Student updateStudent(Student student)
    {
        System.out.println("更新学生:" + student);
        studentMapper.updateStudent(student);
        return student;
    }

    //@CacheEvict 缓存清除
    //属性 key 表示清除指定key的缓存 默认使用方法参数的值
    //属性 allEntries = true 表示清除所有 默认为false
    //属性 beforeInvocation 是否在方法之前执行清除缓存 默认为false,方法之后执行
    //为true的时候无论方法是否会异常都会清除缓存,为false方法异常就不会清除缓存
    @CacheEvict(/*cacheNames = "student"*/)
    public void deleteStudentById(Integer id)
    {
        System.out.println("删除" + id + "号学生...");
        //studentMapper.deleteStudentById(id);
    }

    //@Caching 定义复杂的缓存规则
    @Caching(
            cacheable = {@Cacheable(/*cacheNames = "student",*/ key = "#stu_name")},
            put = {@CachePut(/*cacheNames = "student",*/ key = "#result.id"),
                    @CachePut(/*cacheNames = "student",*/ key = "#result.stu_age")})
    public Student selectStudentByStu_name(String stu_name)
    {
        System.out.println("查询名称为" + stu_name + "的学生...");
        Student student = studentMapper.selectStudentByStu_name(stu_name);
        return student;
    }
}
