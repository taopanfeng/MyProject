package springboot01cache.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student implements Serializable
{
    private Integer id;
    private String stu_name;
    private String stu_age;
    private String grade_id;
}
