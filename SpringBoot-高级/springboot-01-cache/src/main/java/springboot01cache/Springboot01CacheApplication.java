package springboot01cache;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

//开启基于注解的缓存
@EnableCaching
//扫描"springboot01cache.mapper"下的全部mapper文件
@MapperScan("springboot01cache.mapper")
@SpringBootApplication
public class Springboot01CacheApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(Springboot01CacheApplication.class, args);
    }

}
