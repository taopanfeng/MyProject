/*
Navicat MySQL Data Transfer

Source Server         : Docker容器连接
Source Server Version : 80016
Source Host           : 192.168.1.104:3307
Source Database       : tpf

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2019-06-09 16:44:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for GRADE
-- ----------------------------
DROP TABLE IF EXISTS `GRADE`;
CREATE TABLE `GRADE` (
  `ID` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `GRADE_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '年级名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of GRADE
-- ----------------------------
INSERT INTO `GRADE` VALUES ('1', '一年级');
INSERT INTO `GRADE` VALUES ('2', '二年级');
INSERT INTO `GRADE` VALUES ('3', '三年级');

-- ----------------------------
-- Table structure for STUDENT
-- ----------------------------
DROP TABLE IF EXISTS `STUDENT`;
CREATE TABLE `STUDENT` (
  `ID` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `STU_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '外号',
  `STU_AGE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '年龄',
  `GRADE_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '班级ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of STUDENT
-- ----------------------------
INSERT INTO `STUDENT` VALUES ('1', '大牛', '12', '2');
INSERT INTO `STUDENT` VALUES ('2', '二蛋', '76', '2');
INSERT INTO `STUDENT` VALUES ('3', '三驴', '22', '3');
INSERT INTO `STUDENT` VALUES ('4', '四毛', '34', '1');
INSERT INTO `STUDENT` VALUES ('5', '五虎', '19', '1');
