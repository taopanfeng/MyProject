package eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
/*
 * 描述:注册中心
 * first 【时间 2019-06-18 15:15 作者 陶攀峰】
 * second【时间 2020-05-14 13:13:38 作者 陶攀峰】
 */
@EnableEurekaServer//启用注册中心功能
@SpringBootApplication
public class EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

}
