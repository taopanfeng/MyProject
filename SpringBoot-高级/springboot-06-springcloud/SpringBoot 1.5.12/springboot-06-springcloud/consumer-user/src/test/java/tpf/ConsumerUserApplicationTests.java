package tpf;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumerUserApplicationTests
{
    @Autowired
    RestTemplate restTemplate;

    @Test
    public void contextLoads()
    {
        String str = restTemplate.getForObject("http://provider-ticket/ticket", String.class);
        System.out.println(str);
    }

}
