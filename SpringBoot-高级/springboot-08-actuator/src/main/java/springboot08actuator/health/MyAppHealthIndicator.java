package springboot08actuator.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class MyAppHealthIndicator implements HealthIndicator
{
    //自定义检查方法
    @Override
    public Health health()
    {
        //代表健康
        //访问 http://localhost:8080/actuator/health 返回 {"status":"UP"}
        //return Health.up().build();

        //访问 http://localhost:8080/actuator/health 返回 {"status":"DOWN"}
        return Health.down().withDetail("message","应用异常").build();
    }
}
