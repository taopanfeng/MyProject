package com.atguigu.user.service;


import com.alibaba.dubbo.config.annotation.Reference;
import com.atguigu.ticket.service.TicketService;
import org.springframework.stereotype.Service;

@Service
public class UserService
{

    @Reference//    import com.alibaba.dubbo.config.annotation.Reference;
    private TicketService ticketService;

    public void hello()
    {
        String ticket = ticketService.getTicket();
        System.out.println("买到票了：" + ticket);
    }

}
