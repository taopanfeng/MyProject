package panfeng;

import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import panfeng.service.CustomerService;
import panfeng.service.ProviderService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DubboCustomerApplicationTest
{
    @Autowired
    private CustomerService customerService;

    @Reference// import org.apache.dubbo.config.annotation.Reference;
    private ProviderService providerService;

    @Test
    public void test()
    {
        System.out.println(providerService.provider_method());
        System.out.println(customerService.customer_method());
    }

}
