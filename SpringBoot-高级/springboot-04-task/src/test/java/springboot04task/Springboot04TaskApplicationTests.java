package springboot04task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.internet.MimeMessage;
import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Springboot04TaskApplicationTests
{
    @Autowired
    JavaMailSenderImpl mailSender;

    @Test
    public void simple_send_mail()
    {
        //1. 创建一个简单的消息邮件
        SimpleMailMessage message = new SimpleMailMessage();
        //1.1 邮件设置
        message.setSubject("这是标题...");
        message.setText("这是内容...");
        //1.2 发给谁
        message.setTo("1801957499@qq.com");
        //1.3 是谁发的
        message.setFrom("18574150851@163.com");

        //2. 执行发送
        mailSender.send(message);
    }

    @Test
    public void complex_send_mail() throws Exception
    {
        //1. 创建一个复杂的消息邮件
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        //2. multipart=true 表示可以发送附件
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        //2.1 邮件设置
        helper.setSubject("这是标题...");
        //2.2 设置html=true 表示 html标签生效
        helper.setText("<h1 style='color:red'>这是h1红色内容...</h1>", true);
        //2.3 发给谁
        helper.setTo("1801957499@qq.com");
        //2.4 是谁发的
        helper.setFrom("18574150851@163.com");
        //2.5 发送附件
        File file1 = new File("C:\\Users\\Administrator\\Pictures\\连姆尼森.jpg");
        File file2 = new File("C:\\Users\\Administrator\\Pictures\\三体-3BODY-四维空间.jpg");
        helper.addAttachment("图片1.jpg", file1);
        helper.addAttachment("图片2.jpg", file2);

        //3. 执行发送
        mailSender.send(mimeMessage);
    }

}
