package springboot04task.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot04task.service.AsyncService;

@RestController
public class AsyncController
{
    @Autowired
    AsyncService asyncService;

    // 访问 http://localhost:8080/async 会第一时间显示 成功,但是控制台需要等待3秒显示 done...
    @GetMapping("/async")
    public String test_async() throws Exception{
        asyncService.test_async();
        return "成功";
    }
}
