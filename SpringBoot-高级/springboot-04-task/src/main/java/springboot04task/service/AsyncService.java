package springboot04task.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService
{

    @Async//异步方法
    public void test_async() throws Exception{
        Thread.sleep(3000);
        System.out.println("done...");
    }

}
