package springboot04task.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Service
public class TimingService
{
    /*
     * 描述:cron表达式:
     *
     *
     * 字段 允许值 允许的特殊字符
     * 秒 0-59 *,-/
     * 分 0-59 *,-/
     * 小时 0-23 *,-/
     * 日期 1-31 ,-*?/LWC
     * 月份 1-12 *,-/
     * 星期 0-7或MON-SUN(0和7都是代表周日 1-6表示周一到周六) ,-*?/LC#
     *
     * 特殊字符 代表含义
     * , 枚举
     * - 区间
     * * 任意
     * / 步长
     * ? 日/星期冲突匹配
     * L 最后
     * W 工作日
     * C 和calendar联系后计算过的值
     * # 星期 4#2 表示第二个星期四
     * 【时间 2019-06-14 14:33 作者 陶攀峰】
     */

    //second 秒,minute 分,hour 时,day of month 日,month 月,day of week 周几
    //cron = "0 * * * * MON-FRI" 表示周一到周五 每一分钟执行一次
    //cron = "* * * * * MON-FRI" 表示周一到周五 每一秒钟执行一次
    //cron = "0,1,2,3,4 * * * * MON-FRI" 也可以写为 cron = "0-4 * * * * MON-FRI"
    //表示周一到周五 每一分钟允许五次,秒为00 01 02 03 04运行
    //cron = "0/5 * * * * MON-FRI" 表示周一到周五 每5秒执行一次,秒为00 05 10 15 20....运行
    //cron = "0 0/5 14,18 * * ?" 表示每天小时为14或者18 秒为00 分为00 05 10...执行
    //cron = "0 15 10 ? * 1-6" 表示周一到周六的每天10:15:00执行
    //cron = "0 0 2 ? * 6L" 表示每个月的最后一个周六的02:00:00执行
    //cron = "0 0 2 LW * ?" 表示每个月的最后一个工作日的02:00:00执行
    //cron = "0 0 2-4 ? * 1#1" 表示每个月的第一个周一凌晨02:00:00  03:00:00 04:00:00都会执行
    @Scheduled(cron = "0/5 * * * * MON-FRI")
    public void test_timing(){
        String localDateTime = LocalDateTime.now().toString().replace("T", " ");
        System.out.println("定时执行开始,当前时间为 "+ localDateTime);
    }
}
