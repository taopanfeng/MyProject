package springboot03elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import springboot03elasticsearch.pojo.Item;

import java.util.List;
public interface ItemRepository extends ElasticsearchRepository<Item,Long>
{
    /**
     * 根据价格区间查询
     * @param price1
     * @param price2
     * @return
     */
    List<Item> findByPriceBetween(double price1, double price2);
}