package springboot07hotdeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot07HotDeployApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(Springboot07HotDeployApplication.class, args);
    }

}
