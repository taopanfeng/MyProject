package springboot07hotdeploy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class HelloController
{
    @GetMapping("/hello")
    public String hello()
    {
        return "hello";
    }
}
