package com.taopanfeng.drools.workbench;

import org.drools.compiler.kproject.ReleaseIdImpl;
import org.drools.core.io.impl.UrlResource;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.KieScanner;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021-12-02 08:38
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Resource
    private KieBase kieBase;

    @GetMapping("hello")
    public String hello() {
        return "hello";
    }

    @GetMapping("drools_01/{id}")// 程序判断   ===> 成功
    public ResponseEntity<?> drools_01(@PathVariable("id") Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("id", id);

        if (id > 10) {
            map.put("desc", String.format("id[%s] > 10", id));
        } else {
            map.put("desc", String.format("id[%s] <= 10", id));
        }

        return ResponseEntity.ok(map);
    }

    @GetMapping("drools_02/{id}")// 2021-12-02 11:56:18 本地 drl   ===> 成功
    public ResponseEntity<?> drools_02(@PathVariable("id") Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("id", id);

        KieSession kieSession = kieBase.newKieSession();
        kieSession.insert(map);
        kieSession.fireAllRules();
        kieSession.dispose();

        return ResponseEntity.ok(map);
    }

    @GetMapping("drools_03/{id}")// 2021-12-02 11:56:18 远程 drl GAV   ===> 未成功
    public ResponseEntity<?> drools_03(@PathVariable("id") Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("id", id);

        // ReleaseIdImpl releaseId = new ReleaseIdImpl("com.myspace","workbench_01","LATEST");
        ReleaseIdImpl releaseId = new ReleaseIdImpl("com.taopanfeng", "drools-workbench", "1");
        KieServices ks = KieServices.Factory.get();
        KieContainer container = ks.newKieContainer(releaseId);
        KieScanner scanner = ks.newKieScanner(container);


        KieSession kieSession = kieBase.newKieSession();
        kieSession.insert(map);
        kieSession.fireAllRules();
        kieSession.dispose();


        return ResponseEntity.ok(map);
    }

    @GetMapping("drools_04/{id}")// 2021-12-02 11:56:18 远程 drl jar   ===> 成功
    public ResponseEntity<?> drools_04(@PathVariable("id") Integer id) throws Exception {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("id", id);

        //通过此URL可以访问到maven仓库中的jar包
        String url = "http://vm_1:8080/business-central/maven2/com/taopanfeng/drools-workbench/1/drools-workbench-1.jar";

        KieServices kieServices = KieServices.Factory.get();

        //通过Resource资源对象加载jar包
        UrlResource resource = (UrlResource) kieServices.getResources().newUrlResource(url);
        //通过Workbench提供的服务来访问maven仓库中的jar包资源，需要先进行Workbench的认证
        resource.setUsername("admin");
        resource.setPassword("admin");
        resource.setBasicAuthentication("enabled");

        //将资源转换为输入流，通过此输入流可以读取jar包数据
        InputStream inputStream = resource.getInputStream();

        //创建仓库对象，仓库对象中保存Drools的规则信息
        KieRepository repository = kieServices.getRepository();

        //通过输入流读取maven仓库中的jar包数据，包装成KieModule模块添加到仓库中
        KieModule kieModule = repository.addKieModule(kieServices.getResources().newInputStreamResource(inputStream));

        //基于KieModule模块创建容器对象，从容器中可以获取session会话
        KieContainer kieContainer = kieServices.newKieContainer(kieModule.getReleaseId());
        KieSession kieSession = kieContainer.newKieSession();

        kieSession.insert(map);
        kieSession.fireAllRules();
        kieSession.dispose();

        return ResponseEntity.ok(map);
    }
}
