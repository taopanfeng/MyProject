package top.taopanfeng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import top.taopanfeng.myrule.MySelfRule;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-18 14:42
 */
@EnableEurekaClient // 开启Eureka客户端
@SpringBootApplication
//@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = MySelfRule.class)
public class OderMain80
{
    public static void main(String[] args)
    {
        SpringApplication.run(OderMain80.class, args);
    }
}
