package top.taopanfeng.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-20 13:36
 */
@Component
public class MyLB implements LoadBalancer
{
    private AtomicInteger atomicInteger = new AtomicInteger(0);


    //负载均衡算法：第几次请求数 % 服务名称对应的实例列表 = 实际调用实例下标
    // 每次服务重启动后rest接口计数从1开始。
    @Override
    public ServiceInstance instances(List<ServiceInstance> serviceInstances)
    {
        int index = getNext() % serviceInstances.size();
        return serviceInstances.get(index);
    }

    public final int getNext()
    {
        int current;
        int next;

        do
        {
            current = this.atomicInteger.get();
            next = current + 1;
        } while (!this.atomicInteger.compareAndSet(current, next));
        System.out.println("*****第几次访问，次数next: " + next);

        return next;
    }
}
