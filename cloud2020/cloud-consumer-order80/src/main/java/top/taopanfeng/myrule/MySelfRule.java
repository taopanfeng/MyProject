package top.taopanfeng.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述：不能被SpringBootApplication扫描到
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-20 10:51
 */
//@Configuration
public class MySelfRule
{
    //@Bean
    public IRule iRule(){
        // 定义为随机
        return new RandomRule();
    }
}
