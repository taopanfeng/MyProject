package top.taopanfeng.springcloud.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import top.taopanfeng.springcloud.entity.Payment;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-18 11:02
 */
public interface PaymentService
{
    int create(Payment payment);

    Payment getPaymentById(Long id);
}
