package top.taopanfeng.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import top.taopanfeng.springcloud.dao.PaymentDao;
import top.taopanfeng.springcloud.entity.CommonResult;
import top.taopanfeng.springcloud.entity.Payment;
import top.taopanfeng.springcloud.service.PaymentService;

import javax.annotation.Resource;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-18 11:05
 */
@RestController
@Slf4j
public class PaymentController
{
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment)
    {
        int result = paymentService.create(payment);
        log.info("***插入结果：" + payment);
        if (result > 0)
        {
            //return new CommonResult(200, "插入数据库成功", result);
            return new CommonResult(200, "插入数据库成功,serverPort:  " + serverPort, result);
        } else
        {
            return new CommonResult(444, "数据库插入失败", null);
        }
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id)
    {
        //System.out.println(paymentService);
        Payment payment = paymentService.getPaymentById(id);
        log.info("***查询结果：" + payment);
        if (payment != null)
        {
            //return new CommonResult(200, "查询成功", payment);
            return new CommonResult(200, "查询成功,serverPort:  " + serverPort, payment);
        } else
        {
            return new CommonResult(444, "查询失败，没有对应ID：" + id, null);
        }
    }

    @GetMapping(value = "/payment/lb")
    public String getPaymentLB()
    {
        return serverPort;
    }
}
