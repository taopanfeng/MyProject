package top.taopanfeng.springcloud.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import top.taopanfeng.springcloud.entity.CommonResult;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-29 16:27
 */
public class CustomerBlockHandler
{
    public static CommonResult handlerException1(BlockException exception)
    {
        return new CommonResult(4444,"按客戶自定义,global handlerException----1");
    }
    public static CommonResult handlerException2(BlockException exception)
    {
        return new CommonResult(4444,"按客戶自定义,global handlerException----2");
    }
}
