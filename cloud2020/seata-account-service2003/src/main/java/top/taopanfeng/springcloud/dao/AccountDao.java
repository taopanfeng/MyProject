package top.taopanfeng.springcloud.dao;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-06-03 08:29
 */
public interface AccountDao {

    /**
     * 扣减账户余额
     */
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}
