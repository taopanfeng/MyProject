package top.taopanfeng.springcloud.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 描述：配置网关 全局过滤器
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-22 09:44
 */
@Component
@Slf4j
public class MyLogGateWayFilter implements GlobalFilter, Ordered
{

    //配置过滤器
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain)
    {
        log.info("***********come in MyLogGateWayFilter:  " + LocalDateTime.now());

        String username = exchange.getRequest().getQueryParams().getFirst("username");

        if (username == null)
        {
            log.info("*******用户名为null，非法用户，o(╥﹏╥)o");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);//406
            return exchange.getResponse().setComplete();
        }

        return chain.filter(exchange);
    }


    // 加载过滤器的优先级顺序，越小优先级越高。
    // 注意是越小越高
    @Override
    public int getOrder() { return 0; }
}
