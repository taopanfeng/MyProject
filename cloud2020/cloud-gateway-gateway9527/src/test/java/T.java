import java.time.ZonedDateTime;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-22 08:23
 */
public class T
{
    public static void main(String[] args)
    {
        ZonedDateTime zonedDateTime=ZonedDateTime.now();
        System.out.println(zonedDateTime);
        // 2020-05-22T08:24:05.216+08:00[Asia/Shanghai]
    }
}
