package top.taopanfeng.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import top.taopanfeng.springcloud.entity.CommonResult;

import java.math.BigDecimal;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-06-03 08:07
 */
@FeignClient(value = "seata-account-service")
public interface AccountService
{
    @PostMapping(value = "/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}