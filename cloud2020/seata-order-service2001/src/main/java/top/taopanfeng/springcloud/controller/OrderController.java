package top.taopanfeng.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.taopanfeng.springcloud.entity.CommonResult;
import top.taopanfeng.springcloud.entity.Order;
import top.taopanfeng.springcloud.service.OrderService;

import javax.annotation.Resource;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-06-03 08:05
 */
@RestController
public class OrderController
{
    @Resource
    private OrderService orderService;


    @GetMapping("/order/create")
    public CommonResult create(Order order)
    {
        orderService.create(order);
        return new CommonResult(200,"订单创建成功",null);
    }
}
