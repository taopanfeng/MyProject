package top.taopanfeng.springcloud.service;

import top.taopanfeng.springcloud.entity.Order;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-06-03 08:06
 */
public interface OrderService
{
    void create(Order order);
}

