package top.taopanfeng.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-06-03 08:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order
{
    private Long id; //订单编号

    private Long userId; //购买的用户

    private Long productId; //购买的商品

    private Integer count; //购买的数量

    private BigDecimal money; //此订单花了多少钱

    private Integer status; //订单状态：0：创建中；1：已完结
}