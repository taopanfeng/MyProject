package top.taopanfeng.springcloud.service;

import org.springframework.stereotype.Component;
import top.taopanfeng.springcloud.entity.CommonResult;
import top.taopanfeng.springcloud.entity.Payment;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-29 16:47
 */
@Component
public class PaymentFallbackService implements PaymentService
{
    @Override
    public CommonResult<Payment> paymentSQL(Long id)
    {
        return new CommonResult<>(44444,"feign + sentinel,服务降级",new Payment(id,"errorSerial"));
    }
}