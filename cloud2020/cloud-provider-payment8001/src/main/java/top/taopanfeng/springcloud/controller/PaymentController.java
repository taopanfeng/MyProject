package top.taopanfeng.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import top.taopanfeng.springcloud.dao.PaymentDao;
import top.taopanfeng.springcloud.entity.CommonResult;
import top.taopanfeng.springcloud.entity.Payment;
import top.taopanfeng.springcloud.service.PaymentService;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-18 11:05
 */
@RestController
@Slf4j
public class PaymentController
{
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @Resource// import org.springframework.cloud.client.discovery.DiscoveryClient;
    private DiscoveryClient discoveryClient;

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment)
    {
        int result = paymentService.create(payment);
        log.info("***插入结果：" + payment);
        if (result > 0)
        {
            //return new CommonResult(200, "插入数据库成功", result);
            return new CommonResult(200, "插入数据库成功,serverPort:  " + serverPort, result);
        } else
        {
            return new CommonResult(444, "数据库插入失败", null);
        }
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id)
    {
        //System.out.println(paymentService);
        Payment payment = paymentService.getPaymentById(id);
        log.info("***查询结果：" + payment);
        if (payment != null)
        {
            //return new CommonResult(200, "查询成功", payment);
            return new CommonResult(200, "查询成功,serverPort:  " + serverPort, payment);
        } else
        {
            return new CommonResult(444, "查询失败，没有对应ID：" + id, null);
        }
    }

    @GetMapping(value = "/payment/discovery")
    public Object discovery()
    {
        // 获取所有的 服务名
        List<String> services = discoveryClient.getServices();
        for (String element : services)
        {
            log.info("*****element: " + element);
        }

        // 根据服务名获取对应的所有实例
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances)
        {
            log.info(instance.getServiceId() + "\t" + instance.getHost() + "\t" + instance.getPort() + "\t" + instance.getUri());
        }

        return this.discoveryClient;
    }

    @GetMapping(value = "/payment/lb")
    public String getPaymentLB()
    {
        return serverPort;
    }

    @GetMapping(value = "/payment/feign/timeout")
    public String paymentFeignTimeout()
    {
        // 业务逻辑处理正确，但是需要耗费3秒钟
        try
        {
            Thread.sleep(3 * 1000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        return serverPort;
    }
}
