package top.taopanfeng.springcloud.listener;

import org.springframework.messaging.Message;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-25 14:13
 */
public interface ReceiveService
{
    void receive(Message<String> message);
}
