package top.taopanfeng.springcloud.listener;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;

@EnableBinding(value = {DiySink.class})
public class ReceiveServiceImpl implements ReceiveService
{

    @StreamListener(DiySink.input)
    @Override
    public void receive(Message<String> message)
    {
        System.out.println("接收消息" + message.getPayload());
    }
}