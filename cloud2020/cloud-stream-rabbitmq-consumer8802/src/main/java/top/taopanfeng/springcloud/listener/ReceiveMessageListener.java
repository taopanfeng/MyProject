package top.taopanfeng.springcloud.listener;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-25 11:11
 */
//@EnableBinding(Sink.class)
public class ReceiveMessageListener
{
    //@Value("${server.port}")
    private String serverPort;

    //@StreamListener(Sink.INPUT)
    public void input(Message<String> message)
    {
        System.out.println("消费者 "+serverPort+" ----->接受到的消息: "+message.getPayload());
    }
}
