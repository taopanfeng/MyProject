package top.taopanfeng.springcloud.listener;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface DiySink
{
 
    String input = "input1";

    @Input(DiySink.input)
    SubscribableChannel testInput();
}