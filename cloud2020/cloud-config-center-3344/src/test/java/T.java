import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-22 16:22
 */
public class T
{
    public String managePropInfo111(String sys_cmd)
    {
        Process process = null;
        try
        {
            process = Runtime.getRuntime().exec(sys_cmd);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        InputStreamReader out_s = new InputStreamReader(process.getInputStream());
        BufferedReader input_s = new BufferedReader(out_s);
        String line = null;
        try
        {
            line = input_s.readLine();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        //Log.e("aoxi", "output: [" + line + "]");

        return line;
    }


    public String managePropInfo(String sys_cmd)
    {
        Process process = null;
        InputStream inputStream = null;
        BufferedReader input_s = null;
        try
        {
            process = Runtime.getRuntime().exec(sys_cmd);
        } catch (IOException e)
        {
            // 执行命令失败
            e.printStackTrace();
        }
        if (process != null) inputStream = process.getInputStream();
        if (inputStream != null) input_s = new BufferedReader(new InputStreamReader(inputStream));

        String line = null;
        try {
            line = input_s.readLine();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        //Log.e("aoxi", "output: [" + line + "]");
        return line;
    }
}
