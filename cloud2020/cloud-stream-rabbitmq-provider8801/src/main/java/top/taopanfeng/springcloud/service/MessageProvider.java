package top.taopanfeng.springcloud.service;

import org.springframework.cloud.stream.annotation.Output;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-25 08:47
 */
public interface MessageProvider
{
    String send();
}
