package top.taopanfeng.springcloud.mq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-25 14:18
 */
public interface DiySource
{
    String output="output1";

    @Output(DiySource.output)
    MessageChannel send();
}
