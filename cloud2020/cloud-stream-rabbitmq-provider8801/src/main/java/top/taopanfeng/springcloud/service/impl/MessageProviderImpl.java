package top.taopanfeng.springcloud.service.impl;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import top.taopanfeng.springcloud.mq.DiySource;
import top.taopanfeng.springcloud.service.MessageProvider;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-25 08:48
 */
@EnableBinding(DiySource.class) //定义消息的推送管道
public class MessageProviderImpl implements MessageProvider
{
    @Resource(name = DiySource.output)// org.springframework.messaging.MessageChannel;
    private MessageChannel output; // 消息发送管道

    @Override
    public String send()
    {
        String serial = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serial).build());
        System.out.println("*****serial: " + serial);
        return "done..." + serial;
    }
}