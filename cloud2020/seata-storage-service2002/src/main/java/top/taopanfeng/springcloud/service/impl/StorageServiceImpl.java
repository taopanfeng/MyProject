package top.taopanfeng.springcloud.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import top.taopanfeng.springcloud.dao.StorageDao;
import top.taopanfeng.springcloud.service.StorageService;

import javax.annotation.Resource;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-06-03 08:20
 */
@Service
@Slf4j
public class StorageServiceImpl implements StorageService
{

    @Resource
    private StorageDao storageDao;

    /**
     * 扣减库存
     */
    @Override
    public void decrease(Long productId, Integer count) {
        log.info("------->storage-service中扣减库存开始");
        storageDao.decrease(productId,count);
        log.info("------->storage-service中扣减库存结束");
    }
}
