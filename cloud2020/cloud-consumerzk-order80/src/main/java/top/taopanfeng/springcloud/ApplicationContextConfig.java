package top.taopanfeng.springcloud;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 描述
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2020-05-19 15:09
 */
@Configuration
public class ApplicationContextConfig
{
    @Bean
    @LoadBalanced// 负载均衡
    public RestTemplate restTemplate()
    {
        return new RestTemplate();
    }
}
